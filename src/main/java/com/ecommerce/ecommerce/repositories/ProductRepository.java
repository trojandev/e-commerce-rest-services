package com.ecommerce.ecommerce.repositories;

import com.ecommerce.ecommerce.model.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepository extends MongoRepository<Product, Integer> {

    public Product findByName(String name);

}
