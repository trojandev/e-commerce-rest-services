package com.ecommerce.ecommerce.services;

import com.ecommerce.ecommerce.model.Order;
import com.ecommerce.ecommerce.model.Product;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    private static final Logger LOGGER = Logger.getLogger(OrderController.class);

    /**
     * RESTful interface for saving new products into the database.
     * @param order product object
     * @return ResponseEntity with body of product that was attempted to be saved.
     */
    @RequestMapping(value = "/order", method =RequestMethod.POST)
    public ResponseEntity<Order> saveOrder(@RequestBody Order order) {

        LOGGER.info("Saving order: " + order);
        orderService.save(order);
        LOGGER.info("Order was saved!");
        return new ResponseEntity<Order>(order, HttpStatus.OK);
    }

    /**
     * RESTful interface used to get all products in the database.
     * @return List of products.
     */
    @RequestMapping(value = "/order", method =RequestMethod.GET)
    public ResponseEntity<List<Order>> getOrders() {

        LOGGER.info("Getting orders...");
        List<Order> orders = orderService.getOrders();
        LOGGER.info("Successfully retrieved orders.");
        return new ResponseEntity<List<Order>>(orders, HttpStatus.OK);
    }
}
