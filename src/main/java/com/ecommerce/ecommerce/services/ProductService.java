package com.ecommerce.ecommerce.services;


import com.ecommerce.ecommerce.model.Product;
import com.ecommerce.ecommerce.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public boolean save(Product product) {

        productRepository.save(product);
        return true;
    }

    public List<Product> getProducts() {

        return productRepository.findAll();

    }
}
