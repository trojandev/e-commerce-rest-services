package com.ecommerce.ecommerce.services;

import com.ecommerce.ecommerce.model.Product;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class ProductController {

    private static final Logger LOGGER = Logger.getLogger(ProductController.class);

    @Autowired
    private ProductService productService;

    /**
     * RESTful interface for saving new products into the database.
     * @param product product object
     * @return ResponseEntity with body of product that was attempted to be saved.
     */
    @RequestMapping(value = "/product", method =RequestMethod.POST)
    public ResponseEntity<Product> saveProduct(@RequestBody Product product) {

        LOGGER.info("Saving product...");
        productService.save(product);
        LOGGER.info(product.getName() + " was saved!");
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

    /**
     * RESTful interface used to get all products in the database.
     * @return List of products.
     */
    @RequestMapping(value = "/product", method =RequestMethod.GET)
    public ResponseEntity<List<Product>> getProducts() {

        LOGGER.info("Request to getProducts() from the database...");
        List<Product> products = productService.getProducts();
        LOGGER.info("Successfully retrieved products from the database...");
        return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
    }
}
