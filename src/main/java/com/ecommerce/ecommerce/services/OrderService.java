package com.ecommerce.ecommerce.services;


import com.ecommerce.ecommerce.model.Order;
import com.ecommerce.ecommerce.model.Product;
import com.ecommerce.ecommerce.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    public boolean save(Order order) {

        orderRepository.save(order);
        return true;
    }

    public List<Order> getOrders() {
        return orderRepository.findAll();

    }
}
