package com.ecommerce.ecommerce.services;

import com.ecommerce.ecommerce.model.Product;
import com.ecommerce.ecommerce.repositories.ProductRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@TestPropertySource(properties = {"spring.data.mongodb.uri=mongodb://localhost/test"})
@SpringBootTest
public class ProductControllerTest {

    @Autowired
    ProductController productController;

    @Autowired
    ProductRepository productRepository;

    @Before
    public void setup() {
        productRepository.deleteAll();
    }

    @After
    public void after() {
        productRepository.deleteAll();
    }

//    @Test
//    public void createCannedData() {
//        Product product1 = new Product("Baseball", 5);
//        Product product2 = new Product("Basketball", 7);
//        Product product3 = new Product("Football", 6);
//        Product product4 = new Product("Pencil", 3);
//        Product product5 = new Product("Picture Frame", 5);
//        Product product6 = new Product("Stapler", 8);
//        Product product7 = new Product("Laptop", 200);
//        Product product8 = new Product("Television", 400);
//        Product product9 = new Product("Headphones", 150);
//        Product product10 = new Product("Lamp", 25);
//        List<Product> products = new ArrayList<>();
//        products.add(product1);
//        products.add(product2);
//        products.add(product3);
//        products.add(product4);
//        products.add(product5);
//        products.add(product6);
//        products.add(product7);
//        products.add(product8);
//        products.add(product9);
//        products.add(product10);
//        products.forEach(product -> productController.saveProduct(product));
//    }

    @Test
    public void addProduct() {
        Product product = new Product("Baseball", 5);

        ResponseEntity<Product> productResponseEntity = productController.saveProduct(product);
        assertEquals(productResponseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(1, productRepository.findAll().size());

    }

    @Test
    public void getProducts() {
        Product product1 = new Product("Baseball", 5);
        productRepository.save(product1);
        Product product2 = new Product("Basketball", 5);
        productRepository.save(product2);
        Product product3 = new Product("Football", 5);
        productRepository.save(product3);

        ResponseEntity<List<Product>> productResponseEntity = productController.getProducts();
        assertEquals(HttpStatus.OK, productResponseEntity.getStatusCode());
        assertEquals(3, productResponseEntity.getBody().size());

    }
}