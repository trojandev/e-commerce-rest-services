package com.ecommerce.ecommerce.services;

import com.ecommerce.ecommerce.model.Order;
import com.ecommerce.ecommerce.model.Product;
import com.ecommerce.ecommerce.repositories.OrderRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@TestPropertySource(properties = {"spring.data.mongodb.uri=mongodb://localhost/test"})
@SpringBootTest
public class OrderControllerTest {

    @Autowired
    OrderController orderController;

    @Autowired
    OrderRepository orderRepository;

    @Before
    public void setup() {
        orderRepository.deleteAll();
    }

    @After
    public void after() {
        orderRepository.deleteAll();
    }


    @Test
    public void addOrder() {
        Product product1 = new Product("Baseball", 5);
        Product product2 = new Product("Basketball", 7);
        Product product3 = new Product("Football", 6);
        List<Product> products = new ArrayList<>();
        products.add(product1);
        products.add(product2);
        products.add(product3);
        Order order = new Order(products, 18);

        ResponseEntity<Order> productResponseEntity = orderController.saveOrder(order);
        assertEquals(productResponseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(1, orderRepository.findAll().size());

    }

    @Test
    public void getOrders() {
        Product product1 = new Product("Baseball", 5);
        Product product2 = new Product("Basketball", 7);
        Product product3 = new Product("Football", 6);
        List<Product> products1 = new ArrayList<>();
        products1.add(product1);
        products1.add(product2);
        List<Product> products2 = new ArrayList<>();
        products2.add(product3);
        Order order1 = new Order(products1, 12);
        Order order2 = new Order(products2, 6);
        orderRepository.save(order1);
        orderRepository.save(order2);


        ResponseEntity<List<Order>> productResponseEntity = orderController.getOrders();
        assertEquals(HttpStatus.OK, productResponseEntity.getStatusCode());
        assertEquals(2, productResponseEntity.getBody().size());

    }
}